# jhipster

## Lancement

* Dans le dossier source : 
    * Pour le back `./mvnw`
    * Pour le front `yarn start`

## Ajouter un component
(remplacer tous les \*COMPONENT\* par le nom du component)
> 1. Créer un dossier dans **src/main/webapp/app**
> 2. Créer 3 fichiers : **\*COMPONENT\*.component.ts, \*COMPONENT\*.module.ts, \*COMPONENT\*.route.ts**
> 3. Base du fichier **\*COMPONENT\*.route.ts** :
```typescript
import { Route } from '@angular/router';
import { *component* } from './*component*';


export const *COMPONENT*_ROUTE: Route = {
    path: '*path_component*',
    component: *COMPONENT*,
    data: {
        authorities: [],
        pageTitle: '*component*.title'
    }
};
```

> 4. Base du fichier **\*COMPONENT\*.module.ts**
```typescript
import { NgModule } from '@angular/core';
import { *COMPONENT*_ROUTE } from './*component*.route';
import { FirstAppAdminModule } from '../admin/admin.module';
import { FirstAppSharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { RouteController } from '../common/route.controller';
import { CrudService } from 'lap-services';

@NgModule({
    imports: [
        FirstAppSharedModule,
        FirstAppAdminModule,
        LapCrudModule,
        RouterModule.forRoot([ CRUD_ROUTE ], { useHash: true })
    ],
    declarations: [
        CrudComponent
    ],
    providers: [
        CrudService, RouteController
    ]
})
export class *Component*Module {}
```
> 5. Ajouter le module dans la partie **imports** de **app.module.ts** à la racine du dossier **app**

> 6. Pour ajouter la page dans la barre de navigation dans **layouts/navbar/navbar.component.html** :

```html
<li class="nav-item" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}">
    <a class="nav-link" routerLink="*path_component*" (click)="collapseNavbar()">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span jhiTranslate="global.menu.*component*">*Component*</span>
    </a>
</li>
```

> 7. Pour la traduction du menu ajouter dans **i18n/fr/global.json** ajouter dans l'objet menu le nom choisi ainsi que sa traduction.
