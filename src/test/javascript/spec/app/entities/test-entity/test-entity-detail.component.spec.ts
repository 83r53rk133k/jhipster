import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { FirstAppTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TestEntityDetailComponent } from '../../../../../../main/webapp/app/entities/test-entity/test-entity-detail.component';
import { TestEntityService } from '../../../../../../main/webapp/app/entities/test-entity/test-entity.service';
import { TestEntity } from '../../../../../../main/webapp/app/entities/test-entity/test-entity.model';

describe('Component Tests', () => {

    describe('TestEntity Management Detail Component', () => {
        let comp: TestEntityDetailComponent;
        let fixture: ComponentFixture<TestEntityDetailComponent>;
        let service: TestEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FirstAppTestModule],
                declarations: [TestEntityDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TestEntityService,
                    EventManager
                ]
            }).overrideTemplate(TestEntityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestEntityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestEntityService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TestEntity(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.testEntity).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
