import { Component, OnInit } from '@angular/core';
import { PopupService } from './popup.service';
import { RouteController } from '../../common/route.controller';
import { LineForm, TypeField } from '../../common/lap-form/form/form';
import { DataGridHeader } from '../../common/lap-datagrid/lap-datagrid/datagrid.option';
import { crudOptions } from '../../common/lap-crud/crud-options';
import {CrudService} from "../../common/lap-services/crud.service";

@Component({
    selector: 'jhi-author2',
    templateUrl: './author2.component.html'
})
export class Author2Component implements OnInit {

    setForm: LineForm[] = [];

    headerDataGrid: DataGridHeader[] = [];

    crudOptions: crudOptions = new crudOptions();

    constructor(private service: PopupService, private crud: CrudService, private url: RouteController) {
    }

    ngOnInit() {
        this.setHeaderDataGrid();
        this.setFormCrud();
        this.crudOptions.source = this.url.getRoute('java', 'author');
        this.crudOptions.allowCreate = true;
        this.crudOptions.allowDelete = true;
        this.crudOptions.allowEdit = true;
        this.crudOptions.fieldInMsg = 'authorName';
    }

    setHeaderDataGrid() {
        this.headerDataGrid = [
            {field: 'id', header: '#'},
            {field: 'authorName', header: 'Author Name'},
            {field: 'user', header: 'User', sourceField: 'login'}
        ];
    }

    setFormCrud() {
        this.setForm = [
            new LineForm().add('authorName', TypeField.text, {
                placeholder: 'Author Name',
                label: 'Author Name',
                nullable: false
            }),
            new LineForm().add('user', TypeField.select, {
                source: this.url.getRoute('java', 'user'),
                sourceField: ['login'],
                label: 'User'
            })
        ];
    }
}
