import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirstAppSharedModule } from '../../shared';
import { FirstAppAdminModule } from '../../admin/admin.module';
import { authorRoute } from './';
import { Author2Component } from './author2.component';
import { Test } from './test';
import { PopupService } from './popup.service';
import { AuthorService } from './author.service';
import {CrudService} from "../../common/lap-services/crud.service";
import {LapCrudModule} from "../../common/lap-crud/crud.module";

const ENTITY_STATES = [
    ...authorRoute
];


@NgModule({
    imports: [
        FirstAppSharedModule,
        FirstAppAdminModule,
        LapCrudModule,
        RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    ],
    declarations: [
        Author2Component,
        Test
    ],
    entryComponents: [
        Author2Component,
        Test
    ],
    providers: [
        PopupService,
        AuthorService,
        CrudService
    ]
})
export class FirstAppAuthorModule {
}
