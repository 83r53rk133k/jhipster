import {Component} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-test',
    templateUrl: './test.html'
})
export  class Test {
    constructor(public activeModal: NgbActiveModal){}

    clear() {
        this.activeModal.dismiss('cancel');
    }
}
