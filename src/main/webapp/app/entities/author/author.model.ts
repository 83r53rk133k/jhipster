import { User } from '../../shared';
export class Author {
    constructor(
        public id?: number,
        public authorName?: string,
        public user?: User,
    ) {
    }
}
