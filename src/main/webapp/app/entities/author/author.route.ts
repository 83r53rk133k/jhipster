import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';

import { Principal } from '../../shared';
import {Author2Component} from "./author2.component";

export const authorRoute: Routes = [
    {
        path: 'author',
        component: Author2Component,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'firstAppApp.author.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
