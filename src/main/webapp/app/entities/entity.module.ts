import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { FirstAppAuthorModule } from './author/author.module';
import { FirstAppBookModule } from './book/book.module';
import { FirstAppTestEntityModule } from './test-entity/test-entity.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        // FirstAppAuthorModule,
        FirstAppBookModule,
        FirstAppTestEntityModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FirstAppEntityModule {}
