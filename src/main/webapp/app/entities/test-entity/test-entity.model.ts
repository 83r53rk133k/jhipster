import { User } from '../../shared';
export class TestEntity {
    constructor(
        public id?: number,
        public t?: string,
        public user?: User,
    ) {
    }
}
