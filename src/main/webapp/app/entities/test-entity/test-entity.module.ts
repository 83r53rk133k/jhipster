import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirstAppSharedModule } from '../../shared';
import { FirstAppAdminModule } from '../../admin/admin.module';
import {
    TestEntityService,
    TestEntityPopupService,
    TestEntityComponent,
    TestEntityDetailComponent,
    TestEntityDialogComponent,
    TestEntityPopupComponent,
    TestEntityDeletePopupComponent,
    TestEntityDeleteDialogComponent,
    testEntityRoute,
    testEntityPopupRoute,
} from './';

const ENTITY_STATES = [
    ...testEntityRoute,
    ...testEntityPopupRoute,
];

@NgModule({
    imports: [
        FirstAppSharedModule,
        FirstAppAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TestEntityComponent,
        TestEntityDetailComponent,
        TestEntityDialogComponent,
        TestEntityDeleteDialogComponent,
        TestEntityPopupComponent,
        TestEntityDeletePopupComponent,
    ],
    entryComponents: [
        TestEntityComponent,
        TestEntityDialogComponent,
        TestEntityPopupComponent,
        TestEntityDeleteDialogComponent,
        TestEntityDeletePopupComponent,
    ],
    providers: [
        TestEntityService,
        TestEntityPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FirstAppTestEntityModule {}
