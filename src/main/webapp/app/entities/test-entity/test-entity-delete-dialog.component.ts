import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { TestEntity } from './test-entity.model';
import { TestEntityPopupService } from './test-entity-popup.service';
import { TestEntityService } from './test-entity.service';

@Component({
    selector: 'jhi-test-entity-delete-dialog',
    templateUrl: './test-entity-delete-dialog.component.html'
})
export class TestEntityDeleteDialogComponent {

    testEntity: TestEntity;

    constructor(
        private testEntityService: TestEntityService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.testEntityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'testEntityListModification',
                content: 'Deleted an testEntity'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('firstAppApp.testEntity.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-test-entity-delete-popup',
    template: ''
})
export class TestEntityDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testEntityPopupService: TestEntityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.testEntityPopupService
                .open(TestEntityDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
