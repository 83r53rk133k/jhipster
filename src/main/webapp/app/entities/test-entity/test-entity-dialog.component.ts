import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { TestEntity } from './test-entity.model';
import { TestEntityPopupService } from './test-entity-popup.service';
import { TestEntityService } from './test-entity.service';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-test-entity-dialog',
    templateUrl: './test-entity-dialog.component.html'
})
export class TestEntityDialogComponent implements OnInit {

    testEntity: TestEntity;
    authorities: any[];
    isSaving: boolean;

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private testEntityService: TestEntityService,
        private userService: UserService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.testEntity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.testEntityService.update(this.testEntity), false);
        } else {
            this.subscribeToSaveResponse(
                this.testEntityService.create(this.testEntity), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<TestEntity>, isCreated: boolean) {
        result.subscribe((res: TestEntity) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: TestEntity, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'firstAppApp.testEntity.created'
            : 'firstAppApp.testEntity.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'testEntityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-test-entity-popup',
    template: ''
})
export class TestEntityPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testEntityPopupService: TestEntityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.testEntityPopupService
                    .open(TestEntityDialogComponent, params['id']);
            } else {
                this.modalRef = this.testEntityPopupService
                    .open(TestEntityDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
