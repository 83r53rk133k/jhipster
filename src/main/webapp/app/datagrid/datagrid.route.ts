import { Route } from '@angular/router';
import { DatagridComponent } from './datagrid.component';


export const DATAGRID_ROUTE: Route = {
    path: 'datagrid',
    component: DatagridComponent,
    data: {
        authorities: [],
        pageTitle: 'datagrid.title'
    }
};
