import { FirstAppAdminModule } from '../admin/admin.module';
import { FirstAppSharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatagridComponent } from './datagrid.component';
import { DATAGRID_ROUTE } from './datagrid.route';
import { RouteController } from '../common/route.controller';
import {CrudService} from "../common/lap-services/crud.service";
import {LapDataGridModule} from "../common/lap-datagrid/lap-datagrid/datagrid.module";

@NgModule({
    imports: [
        FirstAppSharedModule,
        FirstAppAdminModule,
        LapDataGridModule,
        RouterModule.forRoot([ DATAGRID_ROUTE ], { useHash: true })
    ],
    declarations: [
        DatagridComponent
    ],
    providers: [
        CrudService, RouteController
    ]
})
export class DatagridModule{}
