import { Component, OnInit } from '@angular/core';
import { Principal } from '../shared/auth/principal.service';
import {DataGridHeader, DataGridOption} from "../common/lap-datagrid/lap-datagrid/datagrid.option";

@Component({
    selector: 'jhi-datagrid',
    template: `<lap-datagrid [data]="data" [header]="header" [source]="source" [option]="option" (contextMenuEvent)="test($event)" (dblClickEvent)="dblClick($event)" (toolbarOptionEvent)="toolbar($event)"></lap-datagrid>`
})
export class DatagridComponent implements OnInit {
    data = [];
    header: DataGridHeader[] = [];
    option: DataGridOption;
    currentAccount: any;
    source: any;

    constructor(private principal: Principal) {
        this.header.push({header: '#', field: 'id'});
        this.header.push({header: 'Id utilisateur', field: 'userId'});
        this.header.push({header: 'Titre', field: 'title'});
        this.header.push({header: 'Contenu', field: 'body'});

        this.option = new DataGridOption('normal', true, [{label: 'test', icon: 'fa fa-cog fa-spin '}], [{
            label: 'test',
            icon: 'fa fa-cog fa-spin '
        }], [{label: 'test', icon: 'fa fa-cog fa-spin '}]);

        // this.service.getItems({env: 'java', address: 'http://lce.local/?method=requeteur&requete=exp_infolog&__LIBELSITE__=STJORY&__BIBIOTHEQUE__=fge5xfr01&__DATEMVT__=20170612'}).subscribe(elem => this.data = elem);

        this.source = {env: 'java', address: 'https://jsonplaceholder.typicode.com/posts'};
        // this.service.getItems(this.url.getRoute('shield', params)).subscribe(elem => this.data = elem);
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }
}
