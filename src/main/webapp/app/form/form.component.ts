import {Component, ViewChild, OnInit} from '@angular/core';
import {Principal} from '../shared/auth/principal.service';
import {LineForm, SetButtonForm, TypeButton, TypeField} from "../common/lap-form/form/form";
import {LapFormComponent} from "../common/lap-form/form/form.component";

@Component({
    selector: 'app-lap-form-demo',
    template: `
    <lap-form #form [setForm]="setForm" [setButton]="buttons" (inputCheckAction)="formCheck($event)"></lap-form>`
})

export class FormComponent implements OnInit {

    setForm: LineForm[] = [];
    buttons: SetButtonForm[] = [];
    currentAccount: any;

    jsonForm: any;
    data: any;

    @ViewChild('form') form: LapFormComponent;

    constructor(private principal: Principal) {
        this.setForm = [
            new LineForm()
                .add('nom', TypeField.text, {
                    nullable: false,
                    label: 'Nom'
                })
                .add('prenom', TypeField.text, {
                    nullable: false,
                    label: 'Prénom'
                }),
            new LineForm()
                .add('description', TypeField.textArea)
        ];

        this.buttons = [
            new SetButtonForm().add({
                id: 'valider',
                label: 'Valider',
                command: (event) => this.valider(),
                icon: 'fa-thumbs-up',
                severity: 'success',
                type: TypeButton.button,
                desactiver: true
            })
        ];
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    formCheck(evt) {
        this.form.toggleButtonEnable('valider', evt.formValid);
    }

    valider() {
        console.log(this.form.getValues());
    }
}

