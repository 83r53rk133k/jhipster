import { FirstAppAdminModule } from '../admin/admin.module';
import { FirstAppSharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormComponent } from './form.component';
import { FORM_ROUTE } from './form.route';
import { RouteController } from '../common/route.controller';
import { LapFormModule } from '../common/lap-form/form/form.module';
import {CrudService} from "../common/lap-services/crud.service";


@NgModule({
    imports: [
        FirstAppSharedModule,
        FirstAppAdminModule,
        LapFormModule,
        RouterModule.forRoot([ FORM_ROUTE ], { useHash: true })
    ],
    declarations: [
            FormComponent
    ],
    providers: [
        CrudService, RouteController
    ]
})
export class FormModule{}

