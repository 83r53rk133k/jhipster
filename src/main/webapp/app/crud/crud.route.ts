import { Route } from '@angular/router';
import { CrudComponent } from './crud.component';


export const CRUD_ROUTE: Route = {
    path: 'crud',
    component: CrudComponent,
    data: {
        authorities: [],
        pageTitle: 'crud.title'
    }
};
