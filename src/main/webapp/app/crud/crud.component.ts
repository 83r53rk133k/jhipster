import { Component } from '@angular/core';
import { RouteController } from '../common/route.controller';
import {crudOptions} from "../common/lap-crud/crud-options";
import {CrudService} from "../common/lap-services/crud.service";
import {LineForm, TypeField} from "../common/lap-form/form/form";
import {DataGridHeader} from "../common/lap-datagrid/lap-datagrid/datagrid.option";

@Component({
    selector: 'jhi-crud',
    template: `<lap-crud [headerDataGrid]="headerDataGrid" [setForm]="setForm" [crudOptions]="crudOptions"></lap-crud>`
})
export class CrudComponent {
    setForm: LineForm[] = [];

    headerDataGrid: DataGridHeader[] = [];

    crudOptions: crudOptions = new crudOptions();

    constructor(private crud: CrudService, private url: RouteController) {
    }

    ngOnInit() {
        this.setHeaderDataGrid();
        this.setFormCrud();
        this.crudOptions.source = this.url.getRoute('java', 'author');
        this.crudOptions.allowCreate = true;
        this.crudOptions.allowDelete = true;
        this.crudOptions.allowEdit = true;
        this.crudOptions.fieldInMsg = 'authorName';
    }

    setHeaderDataGrid() {
        this.headerDataGrid = [
            {field: 'id', header: '#'},
            {field: 'authorName', header: 'Author Name'},
            {field: 'user', header: 'User', sourceField: 'login'}
        ];
    }

    setFormCrud() {
        this.setForm = [
            new LineForm().add('authorName', TypeField.text, {
                placeholder: 'Author Name',
                label: 'Author Name',
                nullable: false
            }),
            new LineForm().add('user', TypeField.select, {
                source: this.url.getRoute('java', 'user'),
                sourceField: ['login'],
                label: 'User'
            })
        ];
    }
}
