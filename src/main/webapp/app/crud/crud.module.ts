import { NgModule } from '@angular/core';
import { CRUD_ROUTE } from './crud.route';
import { FirstAppAdminModule } from '../admin/admin.module';
import { FirstAppSharedModule } from '../shared/shared.module';
import { RouteController } from '../common/route.controller';
import { RouterModule } from '@angular/router';
import { CrudComponent } from './crud.component';
import {LapCrudModule} from "../common/lap-crud/crud.module";
import {CrudService} from "../common/lap-services/crud.service";

@NgModule({
    imports: [
        FirstAppSharedModule,
        FirstAppAdminModule,
        LapCrudModule,
        RouterModule.forRoot([ CRUD_ROUTE ], { useHash: true })
    ],
    declarations: [
        CrudComponent
    ],
    providers: [
        CrudService, RouteController
    ]
})
export class CrudModule {}
