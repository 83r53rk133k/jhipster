import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { FirstAppSharedModule, UserRouteAccessService } from './shared';
import { FirstAppHomeModule } from './home/home.module';
import { FirstAppAdminModule } from './admin/admin.module';
import { FirstAppAccountModule } from './account/account.module';
import { FirstAppEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { ModalModule } from 'ngx-bootstrap';

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';
import { DatagridModule } from './datagrid/datagrid.module';
import { FormModule } from './form/form.module';
import { CrudModule } from './crud/crud.module';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        FirstAppSharedModule,
        FirstAppHomeModule,
        FirstAppAdminModule,
        FirstAppAccountModule,
        FirstAppEntityModule,
        ModalModule.forRoot(),
        DatagridModule,
        FormModule,
        CrudModule

    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class FirstAppAppModule {}
