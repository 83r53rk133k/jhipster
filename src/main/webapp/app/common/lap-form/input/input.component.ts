import {
  ChangeDetectorRef, Component, DoCheck, EventEmitter, Input, KeyValueDiffer, KeyValueDiffers, OnDestroy, OnInit,
  Output
} from '@angular/core';
import {TypeField} from "../form/form";
import {CrudService} from "../../lap-services/crud.service";

@Component({
  selector: 'lap-input',
  templateUrl: 'input.component.html',
  styleUrls: ['input.component.css']
})


export class LapInputComponent implements OnInit, DoCheck {


  @Input() field: any = {};
  @Input() editable: Boolean = null;
  TypeField = TypeField;

  @Output() onKeyUpAction: EventEmitter<any> = new EventEmitter(false);
  @Output() onChangeAction: EventEmitter<any> = new EventEmitter(false);
  @Output() onClickAction: EventEmitter<any> = new EventEmitter(false);
  @Output() inputCheckAction: EventEmitter<any> = new EventEmitter(false);

  public inputMsgs: any = [];

  callback: Function;

  oldSource = '';
  oldList: any;
  oldValue: any;
  oldEditable = true;

  constructor(private service: CrudService, private differs: KeyValueDiffers, private cdref: ChangeDetectorRef) {
    this.oldList = this.differs.find({}).create(null);
    this.oldValue = this.differs.find({}).create(null);

    if (this.field.options) {
      if (this.field.options.source) {
        this.loadDataFromSource();
      } else if (this.field.options.list) {
        this.loadDataFromList();
      }
    }
  }

  ngOnInit() {
    this.callback =
      obj => this.handler(obj);

    if (this.editable == false) {
      this.field.options.desactiver = true;
    } else if (this.editable == true) {
      this.field.options.desactiver = false;
    }
  }

  handler(obj: any) {
    if (obj.action == 'change') {
      this.inputCheck(true);
      this.onChangeAction.emit({field: obj.field});
    }

    if (obj.action == 'key') {
      this.inputCheck();
      this.onKeyUpAction.emit({field: obj.field});
    }
  }

  ngDoCheck() {
    if (this.field.options) {
      let change = this.oldList.diff(this.field.options.list);
      if (change) {
        this.loadDataFromList();
      }

      if (this.field.options.source && this.field.options.source != this.oldSource) {
        this.oldSource = this.field.options.source;
        this.loadDataFromSource();
      }
    }

    let valueChanged = this.oldValue.diff(this.field);
    if(valueChanged) {
      this.inputCheck();
    }
  }

  loadDataFromSource() {
    if (this.field.options.source && this.field.type == TypeField.text) {
      this.field.type = TypeField.autoComplete;
    } else if (!this.field.options.source && this.field.type == TypeField.autoComplete) {
      this.field.type = TypeField.text;
    }

    this.service.getItems(this.field.options.source, this.field.options.filter || [])
      .subscribe(res => {
        this.field.list = [];
        for (let data of res) {
          let fullLabel = '';
          let first = true;
          for (let cpt of this.field.options.sourceField) {
            if (!first) {
              fullLabel += ' ';
            }
            first = false;
            fullLabel += data[cpt];
          }
          if (this.field.type == TypeField.text) {
            this.field.list.push(fullLabel);
          } else {
            this.field.list.push({value: data, label: fullLabel});
          }
        }
      });
  }

  loadDataFromList() {
    if (this.field.type != TypeField.text || this.field.type != TypeField.checkBox || this.field.type != TypeField.triState) {
      this.field.list = [];
      for (let cpt of this.field.options.list) {
        this.field.list.push({value: cpt, label: cpt});
      }
    }
  }

  inputCheck(displayerr?: boolean) {
    let err = '';
    if (this.field.options && this.field.options.nullable == false) {
      if (!this.field.value) {
        this.field.valid = false;
        err = 'Veuillez renseigner cette zone.';
      } else {
        this.field.valid = true;
      }
    }

    if ((this.field.type == TypeField.spinner || this.field.type == TypeField.slider) && this.field.value) {
      if (this.field.value < this.field.options.min) {
        this.field.valid = false;
        err = "Minimum autorisé : " + this.field.options.min + ".";
      }

      if (this.field.value > this.field.options.max) {
        this.field.valid = false;
        err = "Maximum autorisé : " + this.field.options.max + ".";
      }

      if(this.field.value > this.field.options.min && this.field.value < this.field.options.max) {
        this.field.valid = true;
      }
    }

    if (this.field.type == TypeField.email && this.field.value) {
      let pattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/i;
      let emailValid = this.field.value.match(pattern);
      if (!emailValid) {
        this.field.valid = false;
        err = 'Veuillez saisir une adresse valide.';
      }else{
        this.field.valid = true;
      }
    }


    if (!this.field.valid) {
      if (displayerr) {
        this.inputMsgs.push({
          severity: 'warn',
          summary: this.field.options.label || this.field.placeholder || this.field.nom,
          detail: err
        });
      }
    }

    this.inputCheckAction.emit({msg: err});
  }

  onClick(event, field) {
    this.onClickAction.emit({evt: event, field: field});
  }
}
