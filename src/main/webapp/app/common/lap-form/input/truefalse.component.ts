import {Component, EventEmitter, Input, Output, ChangeDetectionStrategy} from '@angular/core';


@Component({
  selector: 'lap-input-truefalse',
  templateUrl: 'truefalse.component.html',
  styleUrls: ['input.component.css', 'truefalse.component.css']
})


export class LapInputTrueFalseComponent {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  constructor() {
  }

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }
}
