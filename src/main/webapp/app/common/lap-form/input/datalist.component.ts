import {
  Component, Input, Output, EventEmitter, ChangeDetectionStrategy
} from '@angular/core';

@Component({
  selector: 'lap-input-datalist',
  templateUrl: 'datalist.component.html',
  styleUrls: ['./input.component.css']
})


export class LapInputDatalistComponent {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  private uniqId: number;

  constructor() {
    this.uniqId = Math.round(Math.random() * 9999999);
  }

  keyUp() {
    this.callback({field: this.field.nom, action: 'key'});
  }

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }
}
