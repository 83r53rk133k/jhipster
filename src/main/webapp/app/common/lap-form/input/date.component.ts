import {Component, Input, AfterViewInit, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import * as moment from 'moment';
const $ = require('jquery');
import 'bootstrap-daterangepicker';
import { start } from 'repl';
import {TypeField} from "../form/form";

@Component({
  selector: 'lap-input-date',
  templateUrl: 'date.component.html',
  styleUrls: ['input.component.css']
})


export class LapInputDateComponent implements AfterViewInit {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input('callback') callbackParent: Function;

  private displayDate: string = '';

  constructor() {
  }


  ngAfterViewInit(): void {
    let showTime = (this.field.options ? this.field.options.showTime || false : false);
    let singleDate = true;
    this.displayDate = JSON.parse(JSON.stringify(this.field.value));
    singleDate = this.field.type == TypeField.date;

    let formatDate = 'DD/MM/YYYY';
    if (showTime) {
      formatDate = formatDate + ' HH:mm';
    }

    let startDate = null;
    let endDate = null;

    if(this.displayDate) {
      if (this.field.type == TypeField.dateRange) {
        const splitDate = this.displayDate.split(' - ');
        startDate = splitDate[0];
        endDate = splitDate[1];
      } else {
        startDate = this.displayDate;
        endDate = this.displayDate;
      }
    }

    let settings = {
      locale: {
        cancelLabel: 'Réinitialiser',
        applyLabel: 'Valider',
        format: formatDate
      },
      autoApply: false,
      startDate: startDate || moment().format('DD/MM/YYYY'),
      endDate: endDate || moment().format('DD/MM/YYYY'),
      alwaysShowCalendars: true,
      singleDatePicker: singleDate,
      timePicker: showTime,
      timePicker24Hour: showTime,
      timePickerIncrement: 10,
      autoUpdateInput: false,
      opens: 'left',
      ranges: {
        'Aujourd\'hui': [moment(), moment()],
        'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        '7 derniers jours': [moment().subtract(6, 'days'), moment()],
        '30 derniers jours': [moment().subtract(29, 'days'), moment()],
        'Mois en cours': [moment().startOf('month'), moment().endOf('month')],
        'Mois précédent': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    };

    $('head').append('<style>' + require('./date.component.css') + '</style>');
    const that = this;
    $('#daterangepicker_' + this.field.nom).daterangepicker(settings, this.callback.bind(this))
      .on('apply.daterangepicker', function (ev, picker) {
        if (that.field.type == TypeField.dateRange) {
          that.displayDate = picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY');
        } else {
          that.displayDate = picker.startDate.format('DD/MM/YYYY');
        }
      })
      .on('cancel.daterangepicker', function (ev, picker) {
        that.displayDate = '';
        that.callback();
      });
  }

  private callback(start?: any, end?: any): void {
    this.field.value = [];
    if (start) {
      if (this.field.type == TypeField.dateRange) {
        this.field.value['start'] = start.format(this.field.options.format || 'DD/MM/YYYY');
        this.field.value['end'] = end.format(this.field.options.format || 'DD/MM/YYYY');
      } else {
        this.field.value = start.format(this.field.options.format || 'DD/MM/YYYY');
      }
    }
    this.callbackParent({field: this.field.nom});
  }

  inputChange() {
    this.callbackParent({field: this.field.nom, action: 'change'});
  }

}
