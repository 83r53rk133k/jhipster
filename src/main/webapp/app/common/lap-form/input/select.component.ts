import {
  Component, Input, DoCheck, KeyValueDiffers, OnChanges, SimpleChanges,
  ChangeDetectionStrategy, ChangeDetectorRef, ViewChild
} from '@angular/core';
import {TypeField} from '../form/form';
import { Select2Component } from 'ng2-select2/ng2-select2';

@Component({
  selector: 'lap-input-select',
  templateUrl: 'select.component.html',
  styleUrls: ['./input.component.css']
})


export class LapInputSelectComponent implements OnChanges, DoCheck {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  @ViewChild('select2') select2: Select2Component;

  private options: Select2Options;
  private data: any[] = [];
  private differ: any;

  TypeField = TypeField;

  constructor(private differs: KeyValueDiffers, private cdref: ChangeDetectorRef) {
    this.options = {
      'closeOnSelect': false,
      'minimumResultsForSearch': 10,
      'multiple': true
    };
    this.differ = differs.find({}).create(null);
  }

  ngDoCheck() {
    var changes = this.differ.diff(this.field.list);
    if (changes) {
      this.data = [];
      this.field.list.forEach(elem => this.data.push({id: ''+elem.value.id, text: elem.label}));
      this.cdref.detectChanges();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.field.options && this.field.options.placeholder) {
      this.options.placeholder = this.field.options.placeholder['placeholder'];
    }
    this.options.containerCss = {'border-left': '2px solid ' + (this.field.valid ? 'green' : 'red')};
    this.options.width = '100%';
  }

  inputChange(event?: any) {
    if (event) {
      this.field.value = event;
    }
    this.callback({field: this.field.nom, action: 'change'});
  }
}
