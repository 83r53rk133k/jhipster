import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'primeng/components/common/shared';

import { AutoCompleteModule, ButtonModule, GrowlModule } from 'primeng/primeng';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { Select2Module } from 'ng2-select2';

import { LapInputComponent } from './input.component';
import { LapInputTextComponent } from './text.component';
import { LapInputTextareaComponent } from './textarea.component';
import { LapInputDateComponent } from './date.component';
import { LapInputSelectComponent } from './select.component';
import { LapInputSpinnerComponent } from './spinner.component';
import { LapInputSliderComponent } from './slider.component';
import { LapInputTrueFalseComponent } from './truefalse.component';
import { LapInputPasswordComponent } from './password.component';
import { LapInputEmailComponent } from './email.component';
import { TriStateComponent } from './tri-state.component';
import { LapInputDatalistComponent } from './datalist.component';
import { CrudService } from '../../lap-services/crud.service';

@NgModule({
  declarations: [
    LapInputComponent,
    LapInputTextComponent,
    LapInputTextareaComponent,
    LapInputDateComponent,
    LapInputSelectComponent,
    LapInputSpinnerComponent,
    LapInputSliderComponent,
    LapInputTrueFalseComponent,
    LapInputPasswordComponent,
    LapInputEmailComponent,
    TriStateComponent,
    LapInputDatalistComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    AutoCompleteModule,
    Ng2AutoCompleteModule,
    Select2Module,
    GrowlModule,
    ButtonModule
  ],
  exports: [
    LapInputComponent,
    LapInputTextComponent,
    LapInputTextareaComponent,
    LapInputDateComponent,
    LapInputSelectComponent,
    LapInputSpinnerComponent,
    LapInputSliderComponent,
    LapInputTrueFalseComponent,
    LapInputPasswordComponent,
    LapInputEmailComponent
  ],
  providers: [
      CrudService
  ]
})


export class LapInputFormModule {
}
