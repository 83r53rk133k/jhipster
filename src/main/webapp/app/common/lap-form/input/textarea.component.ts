import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';


@Component({
    selector: 'lap-input-textarea',
    templateUrl: 'textarea.component.html',
    styleUrls: ['input.component.css']
})


export class LapInputTextareaComponent {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  constructor() {}

  keyUp() {
    this.callback({field: this.field.nom, action: 'key'});
  }

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }
}
