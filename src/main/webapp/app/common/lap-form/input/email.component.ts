import {Component, Input, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'lap-input-email',
  templateUrl: 'email.component.html',
  styleUrls: ['input.component.css']
})


export class LapInputEmailComponent {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  constructor() {
  }

  keyUp() {
    this.callback({field: this.field.nom, action: 'key'});
  }

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }

}
