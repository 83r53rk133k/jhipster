import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {TypeField} from "../form/form";

@Component({
  selector: 'lap-input-text',
  templateUrl: 'text.component.html',
  styleUrls: ['input.component.css']
})


export class LapInputTextComponent {

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  @Output() KeyUpAction: EventEmitter<any> = new EventEmitter(false);
  @Output() changeAction: EventEmitter<any> = new EventEmitter(false);

  TypeField = TypeField;

  constructor() {}

  keyUp() {
    this.callback({field: this.field.nom, action: 'key'});
  }

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }

  inputSearch(evt) {

  }
}
