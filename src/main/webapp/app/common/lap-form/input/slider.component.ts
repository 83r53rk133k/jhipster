import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';


@Component({
    selector: 'lap-input-slider',
    templateUrl: 'slider.component.html',
    styleUrls: ['input.component.css']
})


export class LapInputSliderComponent {

  @Input() field: any;
  @Input() callback: Function;

  constructor() {}

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }
}
