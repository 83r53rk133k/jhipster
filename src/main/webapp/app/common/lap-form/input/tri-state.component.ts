import {Component, Input, OnInit, ElementRef, AfterViewInit, DoCheck, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
let $ = require('jquery');

@Component({
  selector: 'lap-input-tri-state',
  templateUrl: 'tri-state.component.html',
  styleUrls: ['input.component.css', 'tri-state.component.css']
})
export class TriStateComponent implements AfterViewInit, DoCheck {
  undefined: boolean = true;
  checked: boolean = false;
  state: boolean = null;

  @Input() field: any;
  @Input() styleClass: string = '';
  @Input() callback: Function;

  constructor(private el: ElementRef) {
  }

  onClick() {
    if (this.undefined) {
      this.undefined = false;
      this.checked = false;
      $('#tri_' + this.field.nom).prop('indeterminate', false);
      $('#tri_' + this.field.nom).prop('checked', false);
      this.state = false;
    } else if (this.checked) {
      this.undefined = true;
      this.checked = null;
      $('#tri_' + this.field.nom).prop('indeterminate', true);
      this.state = null;
    } else if (!this.undefined && !this.checked) {
      this.checked = true;
      this.undefined = false;
      $('#tri_' + this.field.nom).prop('indeterminate', false);
      $('#tri_' + this.field.nom).prop('checked', true);
      this.state = true;
    }
    this.field.value = this.state;
  }

  ngDoCheck() {
    if (this.field.value !== this.state) {
      if (this.field.value === false) {
        this.undefined = false;
        this.checked = false;
        $('#tri_' + this.field.nom).prop('indeterminate', false);
        $('#tri_' + this.field.nom).prop('checked', false);
        this.state = false;
      } else if (this.field.value === null) {
        this.undefined = true;
        this.checked = null;
        $('#tri_' + this.field.nom).prop('indeterminate', true);
        this.state = null;
      } else if (this.field.value === true) {
        this.checked = true;
        this.undefined = false;
        $('#tri_' + this.field.nom).prop('indeterminate', false);
        $('#tri_' + this.field.nom).prop('checked', true);
        this.state = true;
      }
    }
  }

  ngAfterViewInit(): void {
    if (this.field.options.defaultValue['defaultValue']) {
      if (this.field.options.defaultValue['defaultValue'] === 'false') {
        this.undefined = false;
        this.checked = false;
        $('#tri_' + this.field.nom).prop('indeterminate', false);
        $('#tri_' + this.field.nom).prop('checked', false);
        this.state = false;
      } else if (this.field.options.defaultValue['defaultValue'] === 'null') {
        this.undefined = true;
        this.checked = null;
        $('#tri_' + this.field.nom).prop('indeterminate', true);
        this.state = null;
      } else if (this.field.options.defaultValue['defaultValue'] === 'true') {
        this.checked = true;
        this.undefined = false;
        $('#tri_' + this.field.nom).prop('indeterminate', false);
        $('#tri_' + this.field.nom).prop('checked', true);
        this.state = true;
      }
      this.field.value = this.state;
    } else {
      this.field.value = null;
      $('#tri_' + this.field.nom).prop('indeterminate', true);
    }
  }

  inputChange() {
    this.callback({field: this.field.nom, action: 'change'});
  }
}
