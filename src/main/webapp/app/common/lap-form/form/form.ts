import {EventEmitter} from "@angular/core";
export class LineForm {
  field: any[] = [];

  add(nom: string, type: TypeField, options?: FieldForm, value ?: any) {
    this.field.push({nom: nom, type: type, options: options || {}, value: value || null, valid: true});

    let cField = this.field[this.field.length-1];
    if (options) {
      if(options.nullable != null && options.nullable == false) {
        cField.valid = false;
      }
    }
    return this;
  }
}

export interface FieldForm {

  label?: string;
  tailleLabel?: number;
  tailleField?: number;
  taille?: number;

  desactiver?: boolean;
  placeholder?: string;
  nullable?: boolean;
  list?: any;
  source?: any;
  sourceField?: string[];
  value?: string;
  filter?: any[];
  defaultValue?: any;

  maxlength?: number;
  min?: number;
  max?: number;
  step?: number;

  //date
  format?: string;
  showTime?: boolean;
  timeOnly?: boolean;

  //bouton
  command?: Function;
  icon?: string;
  severity?: string;
  eventEmitter?: EventEmitter<any>;
}

export enum TypeField {
  text,
  spinner,
  select,
  multiSelect,
  date,
  dateRange,
  dataList,
  email,
  password,
  slider,
  autoComplete,
  multiAutoComplete,
  textArea,
  triState,
  checkBox,
  action
}

export class SetButtonForm {
  buttons: any = {};

  constructor(taille?: number, align?: string){
    this.buttons = {taille: taille, align: align, buttons: []};
  }

  add(button: ButtonForm) {
    this.buttons.buttons.push(button);
    return this;
  }
}

export interface ButtonForm {
  id?: string;
  label?: string;
  icon: string;
  command: Function;
  desactiver?: boolean;
  type: TypeButton;
  severity: string;
  eventEmitter?: EventEmitter<any>;
  display?: boolean;
}

export enum TypeButton {
  button,
  submit
}
