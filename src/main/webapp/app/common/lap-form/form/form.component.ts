import {
    Component,
    DoCheck,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild,
    AfterViewInit,
    ChangeDetectorRef,
    KeyValueDiffers
} from '@angular/core';
import { SavedSearch } from './saved-search.dto';
import { Router } from '@angular/router';
import { ButtonForm, FieldForm, LineForm, SetButtonForm, TypeButton, TypeField } from './form';
import { LapInputComponent } from '../input/input.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EventManager } from 'ng-jhipster';
import { CheckFormEvent } from './check-form.event';
import { JsonToForm } from './json-to-form';
import {CrudService} from "../../lap-services/crud.service";

@Component({
    selector: 'lap-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.css']
})
export class LapFormComponent implements OnInit, DoCheck, AfterViewInit {
    @Input() savedSearch = false;
    @Input() saveSearchUrl: any;
    @Input() modal = false;
    @Input() setForm: LineForm[] = [];
    @Input() editable: Boolean = null;
    @Input() setButton: SetButtonForm[] = [];
    @Input() jsonForm: any = {};

    @Output() onKeyUpAction: EventEmitter<any> = new EventEmitter(false);
    @Output() onChangeAction: EventEmitter<any> = new EventEmitter(false);
    @Output() inputCheckAction: EventEmitter<any> = new EventEmitter(false);

    @ViewChild('inputs') inputs: LapInputComponent;
    @ViewChild('crudForm') form;

    savedSearchList: SavedSearch[] = [];
    selectedSavedSearch: SavedSearch = null;
    savedSearchName: string;

    TypeField = TypeField;
    TypeButton = TypeButton;

    formMsgs: any = [];
    formValid = false;

    oldEditable: Boolean = null;

    titreFormulaire = '';
    diff: any[] = [];

    constructor(public activeModal: NgbActiveModal,
                private CrudService: CrudService,
                private router: Router,
                private eventManager: EventManager,
                private event: CheckFormEvent,
                private cdRef: ChangeDetectorRef,
                private differ: KeyValueDiffers) {
        this.diff.push(differ.find(this.jsonForm).create(null));
    }

    ngOnInit() {
        if (this.savedSearch) {
            this.loadSavedSearch();
        }
    }

    ngDoCheck() {
        if (this.editable != this.oldEditable) {
            this.oldEditable = this.editable;
            if (this.editable != null) {
                this.toggleEditable();
            }
        }
        let change = this.diff[0].diff(this.jsonForm);
        if (change) {
            let form = new JsonToForm(this.jsonForm);
            this.setForm = form.buildField();
            this.cdRef.detectChanges();
            this.inputs.inputCheck();
        }
    }

    ngAfterViewInit() {
        if (this.inputs) {
            this.inputs.inputCheck();
        }
    }

    toggleEditable() {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => self[pos].options.desactiver = !this.editable));
    }

    setSource(nom: string, source: any, sourceField: string[], filter?: any[]) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].options.source = source;
                self[pos].options.sourceField = sourceField;
                if (filter) {
                    self[pos].options.filter = filter;
                }
            }
        }));
    }

    setValue(nom: string, value: any) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].value = value;
            }
        }));
    }

    setType(nom: string, type: TypeField) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].type = type;
            }
        }));
    }

    setEnabled(nom: string) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].options.desactiver = false;
            }
        }));
    }

    setDisabled(nom: string) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].options.desactiver = true;
            }
        }));
    }

    setValid(nom: string) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].valid = true;
            }
        }));
    }

    setOptions(nom: string, options: FieldForm) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].options = options;
            }
        }));
    }

    resetField(nom: string) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                self[pos].value = null;
            }
        }));
    }

    setButtonEnable(id: string) {
        this.setButton.forEach(setButton => setButton.buttons.buttons.forEach((button, pos, self) => {
            if (button.id == id) {
                self[pos].desactiver = false;
            }
        }));
    }

    setButtonDisable(id: string) {
        this.setButton.forEach(setButton => setButton.buttons.buttons.forEach((button, pos, self) => {
            if (button.id == id) {
                self[pos].desactiver = true;
            }
        }));
    }

    toggleButtonEnable(id: string, enable: boolean) {
        this.setButton.forEach(setButton => setButton.buttons.buttons.forEach((button, pos, self) => {
            if (button.id == id) {
                self[pos].desactiver = !enable;
            }
        }));
    }

    resetFields() {
        this.setForm.forEach(
            elem => elem.field.forEach((field, pos, self) => self[pos].value = null)
        );
    }

    getValue(nom: string) {
        let value;
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) => {
            if (field.nom == nom) {
                value = field.value;
            }
        }));

        return value;
    }

    getValues() {
        let tmp = {};
        this.setForm.forEach(elem => elem.field.forEach(field => tmp[field.nom] = field.value));

        return tmp;
    }

    setValues(obj: any) {
        this.setForm.forEach(elem => elem.field.forEach((field, pos, self) =>
            Object.keys(obj).forEach(key => {
                if (key == field.nom) {
                    self[pos].value = obj[key];
                }
            })
        ));
        if (this.inputs) {
            this.inputs.inputCheck();
        }

        this.cdRef.detectChanges();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    formCheck(displayerr ?: boolean) {
        let formValid = true;
        let err = '';
        this.setForm.forEach(elem => elem.field.forEach(field => {
            if (!field.valid) {
                formValid = false;
                err = 'Le formulaire est incomplet';
            }
        }));

        this.formValid = formValid;

        if (!this.formValid && displayerr) {
            this.formMsgs.push({severity: 'warning', summary: 'Enregistrement impossible', detail: err})
        }

        this.event.publish({formValid: formValid});
        this.eventManager.broadcast({name: 'formCheck', formValid: formValid});
        this.inputCheckAction.emit({msg: err, formValid: this.formValid});
    }

    keyUp(evt) {
        this.onKeyUpAction.emit(evt);
    }

    inputChange(evt) {
        this.formCheck();
        this.onChangeAction.emit(evt);
    }

    buttonFormClick(event, button: ButtonForm) {
        if (button.command) {
            if (!button.eventEmitter) {
                button.eventEmitter = new EventEmitter();
                button.eventEmitter.subscribe(button.command);
            }

            button.eventEmitter.emit({
                originalEvent: event,
                item: button
            });
        }
    }

    inputClick(evt) {
        if (evt.field.options.command) {
            if (!evt.field.options.eventEmitter) {
                evt.field.options.eventEmitter = new EventEmitter();
                evt.field.options.eventEmitter.subscribe(evt.field.options.command);
            }

            evt.field.options.eventEmitter.emit({
                originalEvent: evt.evt,
                item: evt.field
            });
        }
    }

    inputCheck(evt) {
        this.formCheck()
    }

    loadSavedSearch() {
        this.savedSearchList = [];
        this.CrudService.getItems(this.saveSearchUrl).subscribe(res => {
            res.forEach(r => (r['url'] === this.router.url) ? this.savedSearchList.push(new SavedSearch(r)) : null);
        });
    }

    savedSearchExist() {
        return this.savedSearchList.filter(
            elem => {
                return elem.title === this.savedSearchName;
            }
        );
    }

    savedSearchAction(save: boolean) {
        if (this.savedSearchExist().length && !save) {
            this.selectedSavedSearch = this.savedSearchExist()[0];
            this.setForm = this.selectedSavedSearch.form;
        } else {
            const savedSearch = this.savedSearchExist()[0] || new SavedSearch();
            savedSearch.form = this.setForm;
            savedSearch.title = this.savedSearchName;
            savedSearch.url = this.router.url;

            if (!savedSearch.id) {
                this.CrudService.addItem(this.saveSearchUrl, savedSearch).subscribe((data) => {
                    this.formMsgs.push({
                        severity: 'success',
                        summary: 'Recherche enregistrée',
                        detail: 'Enregistrement de la recherche réussit'
                    });
                    this.loadSavedSearch();
                }, (err) => {
                    this.formMsgs.push({
                        severity: 'error',
                        summary: 'Recherche enregistrée',
                        detail: 'Une erreur est survenue lors de l\'enregistrement'
                    });
                });

            } else {
                this.CrudService.updateItem(this.saveSearchUrl, savedSearch).subscribe((data) => {
                    this.formMsgs.push({
                        severity: 'success',
                        summary: 'Recherche enregistrée',
                        detail: 'Mise à jour de l\'enregistrement de la recherche réussit'
                    });
                    this.loadSavedSearch();
                }, (err) => {
                    this.formMsgs.push({
                        severity: 'error',
                        summary: 'Recherche enregistrée',
                        detail: 'Une erreur est survvenu lors de la  mise à jour de l\'enregistrement'
                    });
                });
            }
        }
    }

    savedSearchDel() {
        this.CrudService.deleteItem(this.saveSearchUrl, this.savedSearchExist()[0].id).subscribe((data) => {
            this.formMsgs.push({
                severity: 'info',
                summary: 'Recherche enregistrée',
                detail: 'Recherche enregistrée supprimée'
            });
            this.loadSavedSearch();
        }, (err) => {
            this.formMsgs.push({
                severity: 'error',
                summary: 'Recherche enregistrée',
                detail: 'Une erreur est survenue lors de la suppression de la recherche enregistrée'
            });
        });
    }
}
