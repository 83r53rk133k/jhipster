import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LapFormComponent} from './form.component';

import {LapInputFormModule} from '../input/input.module';
import {GrowlModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/components/button/button';
import {FormsModule} from '@angular/forms';;
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CheckFormEvent } from './check-form.event';
import {CrudService} from "../../lap-services/crud.service";

@NgModule({
    declarations: [
        LapFormComponent
    ],
    imports: [
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        LapInputFormModule,
        GrowlModule,
        ButtonModule
    ],
    exports: [LapFormComponent],
    providers: [CrudService, NgbActiveModal, CheckFormEvent]
})


export class LapFormModule {
}
