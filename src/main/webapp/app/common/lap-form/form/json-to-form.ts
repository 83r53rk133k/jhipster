import { LineForm, TypeField } from './form';

export class JsonToForm {

    private form;

    constructor(form: any) {
        this.form = form;
    }

    buildField(): LineForm[] {
        let tmp: LineForm[] = [];
        this.form.field.forEach(field => tmp.push(new LineForm().add(field.name, (<any>TypeField)[field.type], field)));
        return tmp;
    }
}
