import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Subscription }   from 'rxjs/Subscription';

@Injectable()
export class CheckFormEvent {

    subscribe(observer: (a: any) => void) {
        if (this.subscriptions.find(item => item[0] === observer) !== undefined)
            return;
        let subscription = this.observable.subscribe(observer);
        this.subscriptions.push([observer, subscription]);
    }

    publish(payload: any) {
        this.source.next(payload);
    }

    unsubscribe(observer: (a: any) => void) {
        let foundIndex = this.subscriptions.findIndex(item => item[0] === observer);
        if (foundIndex > -1) {
            let subscription: Subscription = this.subscriptions[foundIndex][1];
            if (subscription) {
                subscription.unsubscribe();
            }

            this.subscriptions.splice(foundIndex, 1);//removes item
        }
    }

    // Observable string source (RxJS)
    private source = new Subject<any>();

    // Observable string streams (RxJS)
    private observable = this.source.asObservable();

    // Cache array of tuples
    private subscriptions: Array<[(a: any) => void, Subscription]> = [];
}
