export class SavedSearch {
  id: number = null;
  title: string;
  url: string;
  private _form: string;
  content: string;

  constructor(obj?: any) {
    if (obj) {
      Object.keys(obj).forEach(key => {
        this[key] = obj[key];
        if (key === 'content') {
          this._form = obj[key];
        }
      });
    }
  }

  get form(): any {
    return JSON.parse(this._form);
  }

  set form(form: any) {
    this._form = JSON.stringify(form);
    this.content = this._form;
  }
}
