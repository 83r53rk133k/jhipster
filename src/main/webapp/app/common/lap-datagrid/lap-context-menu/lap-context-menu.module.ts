import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LapContextMenuComponent, ContextMenuSub } from './lap-context-menu.component';
import { DomHandler } from './domhandler';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        LapContextMenuComponent,
        ContextMenuSub
    ],
    exports: [
        LapContextMenuComponent
    ],
    providers: [DomHandler]
})
export class LapContextMenuModule {
}
