import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LapContextMenuComponent } from './lap-context-menu.component';

describe('LapContextMenuComponent', () => {
  let component: LapContextMenuComponent;
  let fixture: ComponentFixture<LapContextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LapContextMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LapContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
