import {
  Component, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy, Renderer, Inject,
  forwardRef, EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';
import { DomHandler } from './domhandler';

@Component({
  selector: 'lap-context-menu',
  templateUrl: './lap-context-menu.component.html',
  styleUrls: ['./lap-context-menu.component.css']
})
export class LapContextMenuComponent implements AfterViewInit, OnDestroy {

  @Input() model: any[];

  @Input() global: boolean;

  @Input() target: any;

  @Input() style: any;

  @Input() styleClass: string;

  @Input() appendTo: any;

  @ViewChild('container') containerViewChild: ElementRef;

  container: HTMLDivElement;

  visible: boolean;

  documentClickListener: any;

  rightClickListener: any;

  constructor(public el: ElementRef, public domHandler: DomHandler, public renderer: Renderer) {
  }

  ngAfterViewInit() {
    this.container = <HTMLDivElement> this.containerViewChild.nativeElement;

    this.documentClickListener = this.renderer.listenGlobal('body', 'click', () => {
      this.hide();
    });

    if (this.global) {
      this.rightClickListener = this.renderer.listenGlobal('body', 'contextmenu', (event) => {
        this.show(event);
        event.preventDefault();
      });
    } else if (this.target) {
      this.rightClickListener = this.renderer.listen(this.target, 'contextmenu', (event) => {
        this.show(event);
        event.preventDefault();
        event.stopPropagation();
      });
    }

    if (this.appendTo) {
      if (this.appendTo === 'body') {
        document.body.appendChild(this.container);
      } else {
        this.domHandler.appendChild(this.container, this.appendTo);
      }
    }
  }

  show(event?: MouseEvent) {
    this.position(event);
    this.visible = true;
    this.domHandler.fadeIn(this.container, 250);

    if (event) {
      event.preventDefault();
    }
  }

  hide() {
    this.visible = false;
  }

  toggle(event?: MouseEvent) {
    if (this.visible) {
      this.hide();
    } else {
      this.show(event);
    }
  }

  position(event?: MouseEvent) {
    if (event) {
      let left = event.pageX;
      let top = event.pageY;
      let width = this.container.offsetParent ? this.container.offsetWidth : this.domHandler.getHiddenElementOuterWidth(this.container);
      let height = this.container.offsetParent ? this.container.offsetHeight : this.domHandler.getHiddenElementOuterHeight(this.container);
      let viewport = this.domHandler.getViewport();

      //flip
      if (left + width - document.body.scrollLeft > viewport.width) {
        left -= width;
      }

      //flip
      if (top + height - document.body.scrollTop > viewport.height) {
        top -= height;
      }

      //fit
      if (left < document.body.scrollLeft) {
        left = document.body.scrollLeft;
      }

      //fit
      if (top < document.body.scrollTop) {
        top = document.body.scrollTop;
      }

      this.container.style.left = (left - 15) + 'px';
      this.container.style.top = (top - 80) + 'px';
    }
  }

  unsubscribe(item: any) {
    if (item.eventEmitter) {
      item.eventEmitter.unsubscribe();
    }

    if (item.items) {
      for (let childItem of item.items) {
        this.unsubscribe(childItem);
      }
    }
  }

  ngOnDestroy() {
    if (this.documentClickListener) {
      this.documentClickListener();
    }

    if (this.rightClickListener) {
      this.rightClickListener();
    }

    if (this.model) {
      for (let item of this.model) {
        this.unsubscribe(item);
      }
    }

    if (this.appendTo) {
      this.el.nativeElement.appendChild(this.container);
    }
  }

}

@Component({
  selector: 'lap-context-menu-sub',
  templateUrl: 'lap-context-menu-sub.component.html',
  styleUrls: ['./lap-context-menu.component.css']
})
export class ContextMenuSub {

  @Input() item: any[];

  @Input() root: boolean;

  activeItem: any;

  containerLeft: any;

  constructor(public domHandler: DomHandler,
              public router: Router,
              @Inject(forwardRef(() => LapContextMenuComponent))
              public contextMenu: LapContextMenuComponent) {
  }

  onItemMouseEnter(event, item, menuitem) {
    if (menuitem.disabled) {
      return;
    }

    this.activeItem = item;
    let nextElement = item.children[0].nextElementSibling;
    if (nextElement) {
      let sublist = nextElement.children[0];
      sublist.style.zIndex = ++DomHandler.zindex;
      this.position(sublist, item);
    }
  }

  onItemMouseLeave(event, link) {
    this.activeItem = null;
  }

  itemClick(event, item: any) {
    if (item.disabled) {
      event.preventDefault();
      return;
    }

    if (!item.url || item.routerLink) {
      event.preventDefault();
    }

    if (item.command) {
      if (!item.eventEmitter) {
        item.eventEmitter = new EventEmitter();
        item.eventEmitter.subscribe(item.command);
      }

      item.eventEmitter.emit({
        originalEvent: event,
        item: item
      });
    }

    if (item.routerLink) {
      this.router.navigate(item.routerLink);
    }
  }

  listClick(event) {
    this.activeItem = null;
  }

  position(sublist, item) {
    this.containerLeft = this.domHandler.getOffset(item.parentElement)
    let viewport = this.domHandler.getViewport();
    let sublistWidth = sublist.offsetParent ? sublist.offsetWidth : this.domHandler.getHiddenElementOuterWidth(sublist);
    let itemOuterWidth = this.domHandler.getOuterWidth(item.children[0]);

    sublist.style.top = '0px';

    if ((parseInt(this.containerLeft.left, 10) + itemOuterWidth + sublistWidth) > (viewport.width - this.calculateScrollbarWidth())) {
      sublist.style.left = -sublistWidth + 'px';
    } else {
      sublist.style.left = itemOuterWidth + 'px';
    }
  }

  calculateScrollbarWidth(): number {
    let scrollDiv = document.createElement('div');
    scrollDiv.className = 'ui-scrollbar-measure';
    document.body.appendChild(scrollDiv);

    let scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);

    return scrollbarWidth;
  }
}
