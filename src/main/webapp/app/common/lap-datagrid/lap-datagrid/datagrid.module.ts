import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LapContextMenuModule } from '../lap-context-menu/lap-context-menu.module';
import { LapDataGridComponent } from './datagrid.component';
import {CrudService} from "../../lap-services/crud.service";


@NgModule({
  declarations: [
    LapDataGridComponent
  ],
  imports: [
    CommonModule,
    LapContextMenuModule
  ],
  exports: [LapDataGridComponent],
  providers: [CrudService]
})


export class LapDataGridModule {
}
