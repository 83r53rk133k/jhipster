import {
    ChangeDetectorRef, Component, DoCheck, EventEmitter, Input, KeyValueDiffers, OnInit,
    Output
} from '@angular/core';
// import { CrudService } from 'lap-services';
import { DataGridHeader, DataGridOption } from './datagrid.option';
import { CrudService } from '../../lap-services/crud.service';


@Component({
    selector: 'lap-datagrid',
    templateUrl: './datagrid.component.html',
    styleUrls: ['./datagrid.component.css']
})
export class LapDataGridComponent implements OnInit, DoCheck {

    @Input() data = [];
    @Input() source: any;
    @Input() filter: any[] = [];
    @Input() header: DataGridHeader[] = [];
    @Input() option: DataGridOption;

    @Output() contextMenuEvent: EventEmitter<any> = new EventEmitter(false);
    @Output() dblClickEvent: EventEmitter<any> = new EventEmitter(false);
    @Output() toolbarOptionEvent: EventEmitter<any> = new EventEmitter(false);

    private oldData: any;
    private oldSource: any;
    private dataDisplayed: any[];
    private selectedLine: any[] = [];
    private allLineSelected = false;
    private filterDisplayed = false;
    private itemsCM;
    private filtre = [];
    private nbrElemDisplayed = 10;
    private nbrPages = 1;
    private lstPages = [];
    private displayedPage: number = 0;
    private lastfilter: string = '';
    private orderby: any[] = [];
    private allDataDisplayed: number;
    private oldOptions: any;


    constructor(private service: CrudService, private diff: KeyValueDiffers, private cdRef: ChangeDetectorRef) {
        this.oldData = this.diff.find(this.data).create();
        this.oldSource = this.diff.find({}).create();
        this.oldOptions = this.diff.find({}).create();
    }

    ngOnInit() {
        this.displayData(this.nbrElemDisplayed, this.displayedPage);
        if (this.source) {
            this.loadData();
        }
        this.initContextMenu();
    }

    initContextMenu() {
        this.itemsCM = (this.option ? (this.option.typeSelect ? this.option.optionsManyItems : this.option.optionsOneItem) : []);
        this.itemsCM.forEach((elem, pos, self) => self[pos]['command'] = (event) => this.rightClickAction(elem.label));
    }

    ngDoCheck() {
        let changes = this.oldData.diff(this.data);
        if (changes) {
            this.filtreData();
            if (this.header.length == 0) {
                Object.keys(this.data[0]).forEach(key => this.header.push({header: key, field: key}));
            }
        }

        let sourceChanged = this.oldSource.diff(this.source);
        if (sourceChanged) {
            this.loadData();
        }

        let optionsChanged = this.oldOptions.diff(this.option);
        if (optionsChanged) {
            this.initContextMenu();
        }
    }

    loadData() {
        this.service.getItems(this.source).subscribe(elem => this.data = elem);
        this.selectedLine = [];
    }

    toggleSelectAllLine() {
        this.allLineSelected = !this.allLineSelected;
        let selectAll = false;
        if (this.selectedLine.length < this.dataDisplayed.length) {
            selectAll = true;
        }
        this.dataDisplayed.forEach(elem => {
            if (selectAll) {
                if (this.selectedLine.indexOf(elem) == -1) {
                    this.selectAction(elem);
                }
            } else {
                this.selectAction(elem);
            }
        });
    }

    toggleFilter() {
        this.filterDisplayed = !this.filterDisplayed;
        if (!this.filterDisplayed) {
            this.filtre = [];
            this.displayData(this.nbrElemDisplayed, 0);
        }
    }

    dblClickAction(data) {
        this.dataDisplayed.forEach(elem => {
            if (this.selectedLine.indexOf(elem) != -1) {
                this.selectAction(elem);
            }
        });
        this.selectAction(data);
        this.dblClickEvent.emit(data);
    }

    rightClickAction(label: string) {
        this.contextMenuEvent.emit({label: label, data: this.selectedLine});
    }


    selectAction(data: any, rightClick = false) {
        if (this.option.typeSelect) {
            if (!rightClick) {
                if (this.selectedLine.indexOf(data) != -1) {
                    this.selectedLine.splice(this.selectedLine.indexOf(data), 1);
                } else {
                    this.selectedLine.push(data);
                }
            } else {
                this.selectedLine = [];
                this.selectedLine.push(data);
            }
        } else {
            if (this.selectedLine.length == 0) {
                this.selectedLine.push(data);
            } else {
                this.selectedLine.splice(0, 1, data);
            }
        }
    }

    filtreData(field?: string, value?: string) {
        if (field) {
            if (this.filtre.length == 0) {
                this.filtre.push({field: field, test: 'contains', value: value})
            } else if (this.filtre.map(elem => elem.field).indexOf(field) != -1) {
                this.filtre[this.filtre.map(elem => elem.field).indexOf(field)].value = value;
            } else {
                this.filtre.push({field: field, test: 'contains', value: value});
            }
        }
        this.allDataDisplayed = this.service.filtreItems(this.data, this.filtre).length;
        this.displayData(this.nbrElemDisplayed, 0);
    }

    toolbarOption(label: string) {
        this.toolbarOptionEvent.emit({label: label});
    }

    displayData(nbrElem: number, nbrPage: number) {
        this.nbrElemDisplayed = nbrElem;
        this.displayedPage = nbrPage;

        this.nbrPages = this.service.filtreItems(this.data, this.filtre).length / this.nbrElemDisplayed;
        this.nbrPages = Math.ceil(this.nbrPages);
        this.lstPages = [];
        for (let cpt = 0; cpt < this.nbrPages; cpt++) {
            this.lstPages.push(cpt);
        }
        let posStart = this.displayedPage * this.nbrElemDisplayed;
        let posEnd = posStart + this.nbrElemDisplayed;
        if (posEnd > this.data.length) {
            posEnd = this.data.length;
        }
        this.dataDisplayed = this.service.filtreItems(this.data, this.filtre).slice(posStart, posEnd);
        this.dataDisplayed.forEach((elem, pos, self) => Object.keys(elem).forEach(key => self[pos][key] = self[pos][key] instanceof Date ? self[pos][key].toLocaleDateString() : self[pos][key]));
        if (this.dataDisplayed.length > this.nbrElemDisplayed) {
            this.dataDisplayed.length = this.nbrElemDisplayed
        }
        this.cdRef.markForCheck();
    }

    changePage(sens) {
        if (sens == '+') {
            this.displayedPage++;
        }
        if (sens == '-') {
            this.displayedPage--;
        }

        this.displayData(this.nbrElemDisplayed, this.displayedPage);
        this.cdRef.detectChanges();
    }

    exportAllDataToCSV() {
        let excelCtn = '<table border="1">';
        excelCtn += '<tr>';
        for (let header of this.header) {
            excelCtn += '<td ><b>' + header.header + '</b></td>';
        }
        excelCtn += '</tr>';
        for (let data of this.data) {
            excelCtn += '<tr>';
            for (let head of this.header) {
                excelCtn += '<td>';
                if (data[head.field]) {
                    excelCtn += data[head.field];
                }
                excelCtn += '</td>';
            }
            excelCtn += '</tr>';
        }
        excelCtn += '</table>';
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(excelCtn));
    }

    orderBy(field) {
        this.orderby[field] = this.orderby[field] ? (this.orderby[field] == 'asc' ? 'desc' : 'asc') : 'asc';

        for (let t in this.orderby) {
            this.orderby[t] = field == t ? this.orderby[t] : null;
        }

        let asc = this.orderby[field] == 'asc';
        this.dataDisplayed = this.service.sortData(this.data, field, asc);
    }
}
