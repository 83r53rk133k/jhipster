export enum datagridTaille {
  normal,
  petit,
  grand
}

export enum datagridType {
  date,
  boolean,
  array,
  text
}

export class DataGridOption {
  taille: string;
  typeSelect: Boolean;
  optionsToolbar: any[] = [];
  optionsOneItem: any[] = [];
  optionsManyItems: any[] = [];

  constructor(taille = 'normal', typeSelect: Boolean = null, optionsToolbar?: any[], optionsOneItem?: any[], optionsManyItems?: any[]) {
    this.taille = taille;
    this.typeSelect = typeSelect;
    this.optionsToolbar = optionsToolbar || [];
    this.optionsOneItem = optionsOneItem || [];
    this.optionsManyItems = optionsManyItems || [];

  }
}

export interface DataGridHeader {
  header?: string;
  field?: string;
  sourceField?: string;
  format?: datagridType;
  structure?: string;
}
