import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { Shield } from './shield';

@Injectable()
export class CrudService extends Shield {
    private listComparateur = ['=', '!=', '<>', '>', '>=', '<', '<=', 'in', 'notin', 'contains', 'notcontains', 'begins', 'ends'];

    constructor(protected http: Http) {
        super(http);
    }

    deleteItem(url: any, id: number | number[]): Observable<any> {
        if (url.env === 'shield') {
            url.params['id'] = id;
            return this.http.post(url.address, this.objToString(url.params), this.header);
        } else {
            return this.http.delete(`${url.address}/${id}`);
        }
    }

    /* AJOUT D'UN ELEMENT */
    addItem(url: any, obj: any): Observable<any> {
        if (url.env === 'shield') {
            url.params = Object.assign(url.params, obj);
            return this.http.post(url.address, this.objToString(url.params), this.header);
        } else {
            return this.http.post(url.address, obj);
        }
    }

    /* MODIFICATION D'UN ELEMENT */
    updateItem(url: any, obj: any): Observable<any> {
        if (url.env === 'shield') {
            url.params = Object.assign(url.params, obj);
            return this.http.post(url.address, this.objToString(url.params), this.header);
        } else {
            return this.http.put(url.address, obj);
        }
    }

    getItems(url: any, filter = [], soap = false, method = '', data = {}): Observable<any []> {
        if (soap) {
            return this.getXMLfromSOAP(url.address, method, data).map(res => this.filtreItems(res, filter));
        } else {
            if (url.env === 'shield') {
                return this.http.post(url.address, this.objToString(url.params), this.header).map(res => this.filtreItems(res.json(), filter));
            } else {
                return this.http.get(url.address).map(res => this.filtreItems(res.json(), filter));
            }
        }
    }

    getItem(url: any, id: number, table?: string): Observable<any> {
        if (url.env === 'shield') {
            let param = {
                method: 'dbGetItemById',
                table: table,
                id: id
            };
            return this.http.post(url.address, this.objToString(param), this.header).map(res => res.json());
        } else {
            return this.http.get(`${url.address}/${id}`).map(res => res.json());
        }
    }

    //###### SOAP ######

    getXMLfromSOAP(url: string, method: string, params: any): Observable<any> {
        let data = '';
        const keys = Object.keys(params);
        for (const key of keys) {
            data += key + '=' + params[key] + '&';
        }
        data = data.slice(0, -1);
        return this.http.post(`${url}/${method}`, data, {headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})}).map(res => {
            let parser = new DOMParser();
            return this.xmlToJson(parser.parseFromString(res.text(), 'application/xml'));
        });
    }

    xmlToJson(xml) {
        // Create the return object
        let obj = {};

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
                obj = {};
                for (let j = 0; j < xml.attributes.length; j++) {
                    const attribute = xml.attributes.item(j);
                    obj[attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) { // text
            // try to parse text into XMLDocument
            let parser = new DOMParser();
            let res = parser.parseFromString(xml.nodeValue, 'application/xml');
            if (res instanceof XMLDocument) {
                obj = this.xmlToJson(res);
            } else {
                obj = xml.nodeValue;
            }
        }

        // do children
        if (xml.hasChildNodes()) {
            for (let i = 0; i < xml.childNodes.length; i++) {
                const item = xml.childNodes.item(i);
                const nodeName = item.nodeName;
                if (typeof(obj[nodeName]) == 'undefined') {
                    obj[nodeName] = this.xmlToJson(item);
                } else {
                    if (typeof(obj[nodeName].push) == 'undefined') {
                        const old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(this.xmlToJson(item));
                }
            }
        }
        return obj;
    }

    //###### FILTRE ######

    getListComparateur() {
        return this.listComparateur;
    }


    /* TRI DES RESULTATS */
    sortData(data, fieldsort, asc) {
        // let dataSort: any = [];
        // dataSort = datas;
        if (asc) {
            data.sort(function (a, b) {
                return (a[fieldsort] > b[fieldsort]) ? 1 : ((b[fieldsort] > a[fieldsort]) ? -1 : 0);
            });
        }

        if (!asc) {
            data.sort(function (b, a) {
                return (a[fieldsort] > b[fieldsort]) ? 1 : ((b[fieldsort] > a[fieldsort]) ? -1 : 0);
            });
        }
        return data;
    }

    filtreData(result, filters, cond = null) {
        for (let filter in filters) {
            let field = filters[filter].field;
            let test = filters[filter].test;
            let value = filters[filter].value;

            if (result[field] instanceof Date) {
                cond = cond != null ? cond && this.switchCaseTest(result[field].toLocaleDateString(), test, moment(value).format('L')) : this.switchCaseTest(result[field].toLocaleDateString(), test, moment(value).format('L'));
            } else if (result[field] instanceof Boolean) {
                cond = cond != null ? cond && this.switchCaseTest((result[field] ? 'oui' : 'non'), test, value) : this.switchCaseTest((result[field] ? 'oui' : 'non'), test, value);
            } else if ((result[field] instanceof Object && (result[field] ! instanceof Date || result[field] ! instanceof Boolean)) || result[field] instanceof Array) {
                for (let prop in result) {
                    cond = cond != null ? cond && this.filtreData(result[prop], filters, cond) : this.filtreData(result[prop], filters, cond);
                }
            } else {
                cond = cond != null ? cond && this.switchCaseTest(result[field], test, value) : this.switchCaseTest(result[field], test, value);
            }
        }
        return cond;
    }

    private switchCaseTest(data, test, value) {
        if (typeof data === 'string') {
            data = data.toLowerCase();
            value = value.toLowerCase();
        }

        switch (test) {
            case '=':
                return data == value;

            case '!=':
            case '<>':
                return data != value;

            case '<':
                return data < value;

            case '<=':
                return data <= value;

            case '>':
                return data > value;

            case '>=':
                return data >= value;

            case 'range':
                return data >= value[0] && data <= value[1];

            case 'in':
                return value.includes(data);

            case 'notin':
                return !value.includes(data);

            case 'contains':
                return (data + '').includes(value);

            case 'notcontains':
                return !(data + '').includes(value);

            case 'begins':
                return (data + '').substr(0, value.length) == value;

            case 'ends':
                return (data + '').substr(0 - value.length) == value;

            default:
                return false;
        }
    }

    /* FILTRE DES ELEMENTS SI BESOIN */
    filtreItems(res, filters) {
        return filters.length > 0 ? res.filter(elem => this.filtreData(elem, filters)) : res;
    }
}
