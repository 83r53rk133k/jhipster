export interface PhpInterfaceRequeteur {
    method: 'executeRequete';
    requete: string;
    params: any;
}


export interface PhpInterfaceDbquery {
    method: 'dbquery';
    driver: string;
    host: string;
    port: null | 5432 | 3306;
    user: string;
    password: string;
    dbname: string;
    schema: string;
}
