import { Http, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

export class Shield {
    protected header: RequestOptionsArgs;

    constructor(protected http: Http) {
        this.header = {headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})}
    }

    getFormulaire(url, type: 'requete' | 'formulaire', formulaire: string): Observable<any> {
        let param = {method: (type === 'requete' ? 'getFormulaireRequete' : 'getFormulaire')};
        param[type === 'requete' ? 'requete' : 'formulaire'] = formulaire;

        return this.http.post(url.address, this.objToString(param), this.header).map(res => res.json());
    }

    getCrud(url, crud): Observable<any> {
        return this.http.post(url.address, this.objToString({method: 'getCrud', crud: crud}), this.header).map(res => res.json());
    }


    objToString(obj: any) : string {
        let tmp = '';
        Object.keys(obj).forEach(key => {
            if(typeof obj[key] === 'object') {
                tmp += this.objToString(obj[key]);
            }else{
                tmp += key+'='+obj[key]+'&';
            }
        });
        return tmp;
    }
}
