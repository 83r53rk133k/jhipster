import { Component } from '@angular/core';

@Component({
    selector: 'lap-growl',
    template: `<alert *ngFor="let msg of alerts" type="msg.type" dismissOnTimeout="msg.timeout">
    {{msg.msg}}
</alert>`
})
export class GrowlComponent {
    alerts: any[];


}
