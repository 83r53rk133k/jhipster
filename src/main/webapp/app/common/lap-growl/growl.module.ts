import { GrowlComponent } from './growl.component';
import { NgModule } from '@angular/core/core';
import { AlertModule } from 'ngx-bootstrap';

@NgModule({
    imports: [
        AlertModule.forRoot()
    ],
    declarations: [
        GrowlComponent
    ]
})
export class GrowlModule{}
