import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class PopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router

    ) {}

    open(component: Component): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        return this.bookModalRef(component);
    }

    bookModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: true});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
