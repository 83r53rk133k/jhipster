import {Component} from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {crudOptions} from "./crud-options";
import { ConfirmDeleteEvent } from './confirm-delete.event';

@Component({
    selector: 'lap-crud-delete-confirm',
    templateUrl: './confirmDelete.html'
})
export class ConfirmDelete {

    listItem: any[] = [];
    crudOptions: crudOptions = new crudOptions;

    constructor(public activeModal: NgbActiveModal, private event: ConfirmDeleteEvent) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirm() {
        this.activeModal.dismiss('done');
        this.event.publish({confirm: 'OK'});
    }
}
