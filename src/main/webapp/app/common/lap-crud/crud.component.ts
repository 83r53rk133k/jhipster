import {
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    ViewChild,
    OnDestroy,
    KeyValueDiffers,
    DoCheck
} from '@angular/core';
import { LineForm, SetButtonForm, TypeButton } from '../lap-form/form/form';
import { crudOptions } from './crud-options';
import { ModalDirective } from 'ngx-bootstrap';
import { LapFormComponent } from '../lap-form/form/form.component';
import { LapDataGridComponent } from '../lap-datagrid/lap-datagrid/datagrid.component';
import { PopupService } from './popup.service';
import { DataGridHeader, DataGridOption } from '../lap-datagrid/lap-datagrid/datagrid.option';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDelete } from './confirmDelete';
import { CrudService } from '../lap-services/crud.service';
import { CheckFormEvent } from '../lap-form/form/check-form.event';
import { ConfirmDeleteEvent } from './confirm-delete.event';
import { JsonToForm } from '../lap-form/form/json-to-form';
import { Observable } from 'rxjs/Observable';
import { AlertService } from 'ng-jhipster';


@Component({
    selector: 'lap-crud',
    templateUrl: './crud.component.html',
    styleUrls: ['./crud.component.css']
})


export class LapCrudComponent implements OnInit, OnDestroy, DoCheck {

    @Input() headerDataGrid: DataGridHeader[] = [];
    @Input() setForm: LineForm[] = [];
    @Input() crudOptions: crudOptions = new crudOptions;
    @Input() jsonForm: any;

    @ViewChild('editModal') public editModal: ModalDirective;
    @ViewChild('confirmModal') public confirmModal: ModalDirective;
    @ViewChild('datagrid') datagrid: LapDataGridComponent;

    option: DataGridOption;
    header: DataGridHeader[] = [];
    pageTitle = '';
    listItem: any[] = [];
    currentItem: number = 0;

    formButton: SetButtonForm[] = [];

    edition = true;
    creation = false;

    formValid: boolean = false;

    data = {};

    alertMsgs: any = [];

    instance: any;
    form: NgbModalRef;
    del: NgbModalRef;

    diff: any[] = [];

    private eventHandlerForm = (a) => {
        this.checkForm(a);
    }

    private eventHandlerDelete = (a) => {
        this.deleteItemConfirm();
    }

    constructor(private service: CrudService,
                private cdref: ChangeDetectorRef,
                private popup: PopupService,
                private eventForm: CheckFormEvent,
                private eventDelete: ConfirmDeleteEvent,
                private differ: KeyValueDiffers,
                private alert: AlertService) {
        this.diff.push(this.differ.find(this.crudOptions).create(null));

    }

    ngDoCheck() {
        let change = this.diff[0].diff(this.crudOptions);
        if (change) {
            this.ngOnInit();
        }
    }

    ngOnInit() {
        this.setDataGridHeader();

        this.option = new DataGridOption('normal');
        this.option.typeSelect = this.crudOptions.multiSelect;

        if (this.crudOptions.allowCreate) {
            this.option.optionsToolbar = [{label: 'Créer', icon: 'fa fa-file-o'}];
        }

        this.option.optionsManyItems.push({label: 'Visualiser', icon: 'fa fa-search', fonction: 'showItems'});
        this.option.optionsOneItem.push({label: 'Visualiser', icon: 'fa fa-search', fonction: 'showItems'});

        if (this.crudOptions.allowEdit) {
            this.option.optionsManyItems.push({label: 'Modifier', icon: 'fa fa-pencil', fonction: 'editItems'});
            this.option.optionsOneItem.push({label: 'Modifier', icon: 'fa fa-pencil', fonction: 'editItems'});
        }

        if (this.crudOptions.allowDelete) {
            this.option.optionsManyItems.push({label: 'Supprimer', icon: 'fa fa-trash', fonction: 'delItems'});
            this.option.optionsOneItem.push({label: 'Supprimer', icon: 'fa fa-trash', fonction: 'delItems'});
        }

        this.pageTitle = this.crudOptions.title;
        this.setFormButton();
        this.eventForm.subscribe(this.eventHandlerForm);
        this.eventDelete.subscribe(this.eventHandlerDelete);
        this.eventListener()
    }

    eventListener() {
        if (this.instance && this.instance.inputCheckAction) {
            this.instance.inputCheckAction.subscribe((response) => this.checkForm(response));
        }
    }

    ngOnDestroy() {
        this.eventForm.unsubscribe(this.eventHandlerForm);
        this.eventDelete.unsubscribe(this.eventHandlerDelete);
    }

    setDataGridHeader() {
        this.header = this.headerDataGrid;
    }

    dblClick(evt) {
        this.form = this.popup.open(LapFormComponent);
        this.instance = this.form.componentInstance;
        this.showItems([evt]);
    }

    toolbar(evt) {
        this.form = this.popup.open(LapFormComponent);
        this.instance = this.form.componentInstance;
        switch (evt.label) {
            case 'Créer':
                this.createItem();
                break;
        }
    }

    cmAction(evt) {

        switch (evt.label) {
            case 'Visualiser':
                this.form = this.popup.open(LapFormComponent);
                this.instance = this.form.componentInstance;
                this.showItems(evt.data);
                break;
            case 'Modifier':
                this.form = this.popup.open(LapFormComponent);
                this.instance = this.form.componentInstance;
                this.editItems(evt.data);
                break;
            case 'Supprimer':
                this.delItems(evt.data);
                break;
        }
    }

    createItem() {
        this.edition = true;
        this.creation = true;
        this.instance.modal = true;
        if (this.jsonForm) {
            this.instance.jsonForm = this.jsonForm;
        } else {
            this.instance.setForm = this.setForm;
        }
        this.instance.titreFormulaire = 'Création d\'un(e) ' + this.crudOptions.libelInMsg;
        this.setFormButton();
        this.instance.resetFields();
        this.instance.editable = this.edition;
    }

    showItems(listItems) {
        this.edition = false;
        this.creation = false;
        this.listItem = listItems;
        this.currentItem = 0;
        this.instance.modal = true;
        if (this.jsonForm) {
            let form = new JsonToForm(this.jsonForm);
            this.instance.setForm = form.buildField();
        } else {
            this.instance.setForm = this.setForm;
        }
        this.instance.titreFormulaire = '#' + this.listItem[this.currentItem].id + ' Visualisation de \'' + this.listItem[this.currentItem][this.crudOptions.fieldInMsg] + '\'';
        this.setFormButton();
        this.instance.setValues(this.listItem[this.currentItem]);
        this.instance.editable = this.edition;
    }

    editItems(listItems) {
        this.edition = true;
        this.creation = false;
        this.listItem = listItems;
        this.currentItem = 0;
        this.instance.modal = true;
        if (this.jsonForm) {
            let form = new JsonToForm(this.jsonForm);
            this.instance.setForm = form.buildField();
        } else {
            this.instance.setForm = this.setForm;
        }
        this.instance.titreFormulaire = '#' + this.listItem[this.currentItem].id + ' Modification de \'' + this.listItem[this.currentItem][this.crudOptions.fieldInMsg] + '\'';
        this.setFormButton();
        this.instance.setValues(this.listItem[this.currentItem]);
        this.instance.editable = this.edition;
    }

    delItems(listItems) {
        this.listItem = listItems instanceof Array ? listItems : [listItems];

        if (this.crudOptions.confirmDelete) {
            if (this.form) {
                this.form.close();
                this.form.result.then(() => {
                        this.del = this.popup.open(ConfirmDelete);
                        let instance = this.del.componentInstance;
                        instance.listItem = this.listItem;
                        instance.crudOptions = this.crudOptions;
                    },
                    err => {
                        this.del = this.popup.open(ConfirmDelete);
                        let instance = this.del.componentInstance;
                        instance.listItem = this.listItem;
                        instance.crudOptions = this.crudOptions;
                    });
            } else {
                this.del = this.popup.open(ConfirmDelete);
                let instance = this.del.componentInstance;
                instance.listItem = this.listItem;
                instance.crudOptions = this.crudOptions;
            }
        }
        else {
            this.deleteItemConfirm();
        }
    }

    deleteItemConfirm() {
        let nbDel = 0;
        let observables = [];
        for (let cpt of this.listItem) {
            if (this.checkDeleteConstraints(cpt)) {
                if (!this.crudOptions.shield) {
                    observables.push(this.service.deleteItem(this.crudOptions.source, cpt.id));
                } else {

                    let url = Object.assign({}, this.crudOptions.source);
                    url.params.method = this.crudOptions.shield.deleteItem;
                    observables.push(this.service.deleteItem(url, cpt.id));
                }
            }
        }
        Observable.forkJoin(observables).subscribe(res => {
            res.forEach(
                data => nbDel++,
                err => this.crudHandleError()
            );
            this.growMessage('D', nbDel);
        });
    }


    navigateItem(pos: number) {
        this.currentItem = pos;
        this.instance.setValues(this.listItem[this.currentItem]);
        if (this.edition) {
            this.instance.titreFormulaire = '#' + this.listItem[this.currentItem].id + ' Modification de \'' + this.listItem[this.currentItem][this.crudOptions.fieldInMsg] + '\'';
        } else {
            this.instance.titreFormulaire = '#' + this.listItem[this.currentItem].id + ' Visualisation de \'' + this.listItem[this.currentItem][this.crudOptions.fieldInMsg] + '\'';
        }
        this.setFormButton();
    }

    setEditable() {
        this.edition = true;
        this.instance.editable = this.edition;
        this.instance.titreFormulaire = '#' + this.listItem[this.currentItem].id + ' Modification de \'' + this.listItem[this.currentItem][this.crudOptions.fieldInMsg] + '\'';
        this.setFormButton();
    }

    setUneditable() {
        this.edition = false;
        this.instance.editable = this.edition;
        this.instance.titreFormulaire = '#' + this.listItem[this.currentItem].id + ' Visualisation de \'' + this.listItem[this.currentItem][this.crudOptions.fieldInMsg] + '\'';
        this.setFormButton();
    }

    checkForm(evt) {
        this.formValid = evt.formValid;
        if (this.creation) {
            this.instance.toggleButtonEnable('creation', evt.formValid)
        }
        if (this.edition && !this.creation) {
            this.instance.toggleButtonEnable('modification', evt.formValid)
        }
        this.cdref.detectChanges();
    }

    saveItem() {
        if (this.formValid) {
            this.data = this.instance.getValues();
            this.instance.resetFields();
            this.form.close();

            if (this.creation) {
                if (!this.crudOptions.shield) {
                    this.service.addItem(this.crudOptions.source, this.data).subscribe(
                        data => this.crudHandleNoError('A'),
                        err => this.crudHandleError()
                    );
                } else {
                    let url = Object.assign({}, this.crudOptions.source);
                    url.params.method = this.crudOptions.shield.createItem;
                    this.service.addItem(url, this.data).subscribe(
                        data => this.crudHandleNoError('A'),
                        err => this.crudHandleError()
                    );
                }
            } else {
                if (this.checkEditConstraints(this.data)) {
                    if (!this.crudOptions.shield) {
                        this.data['id'] = this.listItem[this.currentItem].id
                        this.service.updateItem(this.crudOptions.source, this.data).subscribe(
                            data => this.crudHandleNoError('E'),
                            err => this.crudHandleError()
                        );
                    } else {
                        let url = Object.assign({}, this.crudOptions.source);
                        this.data['id'] = this.listItem[this.currentItem].id
                        url.params.method = this.crudOptions.shield.updateItem;
                        this.service.updateItem(url, this.data).subscribe(
                            data => this.crudHandleNoError('E'),
                            err => this.crudHandleError()
                        );
                    }
                }
            }
        }
    }

    checkDeleteConstraints(item: any) {
        let canDelete = true;
        let msg = '';
        for (let constraint of this.crudOptions.deleteConstraints) {
            if (canDelete) {
                if (item[constraint.field] != constraint.condition) {
                    canDelete = false;
                    msg = constraint.message;
                }
            }
        }

        if (!canDelete) {
            this.alertMsgs.push({severity: 'warn', summary: 'Opération impossible', detail: msg});
            return false;
        }
        return true;
    }

    checkEditConstraints(item: any) {
        let canDelete = true;
        let msg = '';
        for (let constraint of this.crudOptions.deleteConstraints) {
            if (canDelete) {
                if (item[constraint.field] != constraint.condition) {
                    canDelete = false;
                    msg = constraint.message;
                }
            }
        }

        if (!canDelete) {
            this.alertMsgs.push({severity: 'warn', summary: 'Opération impossible', detail: msg});
            return false;
        }
        return true;
    }

    setFormButton() {
        this.formButton = [];
        this.formButton.push(new SetButtonForm(2, 'mr-auto')
            .add({
                label: 'Abandonner',
                icon: 'fa-close',
                severity: 'warning',
                command: (event) => this.setUneditable(),
                type: TypeButton.button,
                display: this.edition && !this.creation
            })
            .add({
                label: 'Fermer',
                icon: 'fa-close',
                severity: 'warning',
                command: (event) => this.form.close(),
                type: TypeButton.button,
                display: !this.edition || this.creation
            })
        );

        if (!this.creation) {
            if (this.listItem.length > 1) {
                this.formButton.push(new SetButtonForm(1, 'mr-auto')
                    .add({
                        icon: 'fa-chevron-left',
                        severity: 'info',
                        command: (event) => this.navigateItem(this.currentItem - 1),
                        type: TypeButton.button,
                        desactiver: !(this.currentItem > 0),
                    })
                    .add({
                        icon: 'fa-chevron-right',
                        severity: 'info',
                        command: (event) => this.navigateItem(this.currentItem + 1),
                        type: TypeButton.button,
                        desactiver: !(this.currentItem < this.listItem.length - 1),
                    })
                );
            }
        }

        this.formButton.push(new SetButtonForm(7, 'ml-auto')
            .add({
                icon: 'fa-trash',
                command: (event) => this.cmAction({label: 'Supprimer', data: this.listItem[this.currentItem]}),
                type: TypeButton.button,
                severity: 'danger',
                label: 'Supprimer',
                display: this.crudOptions.allowDelete && !this.creation
            })
            .add({
                icon: 'fa-pencil',
                label: 'Modifier',
                type: TypeButton.button,
                command: (event) => this.setEditable(),
                severity: 'success',
                display: this.crudOptions.allowEdit && !this.edition
            })
            .add({
                id: 'modification',
                label: 'Enregistrer',
                icon: 'fa-save',
                severity: 'success',
                command: (event) => this.saveItem(),
                type: TypeButton.button,
                display: this.edition && !this.creation
            })
            .add({
                id: 'creation',
                icon: 'fa-save',
                label: 'Créer',
                command: (event) => this.saveItem(),
                type: TypeButton.button,
                severity: 'success',
                display: this.creation
            })
        );

        if (this.instance) {
            this.instance.setButton = this.formButton;
        }
        this.cdref.detectChanges();
    }

    crudHandleError() {
        this.alertMsgs.push({
            severity: 'error',
            summary: 'Erreur',
            detail: 'Une erreur est intervenue. Traitement interrompu'
        });
    }

    crudHandleNoError(action?: string) {
        if (action) {
            this.growMessage(action);
        }
    }

    growMessage(type, nb?: number) {
        let severity = 'info'; // success, info, warning, danger
        let summary = '';
        let detail = '';

        switch (type) {
            case 'A':
                severity = 'success';
                summary = 'Création';
                detail = this.data[this.crudOptions.fieldInMsg] + ' a été créé(e).';
                break;

            case 'E':
                severity = 'success';
                summary = 'Modification';
                detail = this.data[this.crudOptions.fieldInMsg] + ' a été modifié(e).';
                break;

            case 'D':
                severity = 'success';
                summary = 'Suppression';
                if (this.listItem.length == 1) {
                    detail = this.listItem[0][this.crudOptions.fieldInMsg] + ' a été supprimé(e).';
                } else {
                    detail = nb + ' ' + this.crudOptions.libelPlurielInMsg + ' ont été supprimé(e)s.';
                }
                break;
        }
        this.listItem = [];
        if(this.crudOptions.shield) {
            this.crudOptions.source.params.method = this.crudOptions.shield.getAllItems;
        }
        this.datagrid.loadData();

        if (detail) {
            this.alert.success(detail, null, null);
            this.alertMsgs.push({severity: severity, summary: summary, detail: detail});
        }
    }
}
