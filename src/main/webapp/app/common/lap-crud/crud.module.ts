import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LapCrudComponent } from './crud.component';
import { FormsModule }   from '@angular/forms';
import { SharedModule } from 'primeng/components/common/shared';

import { SplitButtonModule, DataTableModule, PaginatorModule, ContextMenuModule } from 'primeng/primeng'
import { ToolbarModule, MessagesModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/components/button/button';

import { LapFormModule } from '../lap-form/form/form.module';
import { LapDataGridModule } from '../lap-datagrid/lap-datagrid/datagrid.module';
import { PopupService } from './popup.service';
import { LapFormComponent } from '../lap-form/form/form.component';
import { DataProvider } from '../data-provider';
import { ConfirmDelete } from './confirmDelete';
import { CrudService } from '../lap-services/crud.service';
import { CheckFormEvent } from '../lap-form/form/check-form.event';
import { ConfirmDeleteEvent } from './confirm-delete.event';
import { AlertModule } from 'ngx-bootstrap';

@NgModule({
    declarations: [
        LapCrudComponent,
        ConfirmDelete
    ],
    imports: [
        CommonModule,
        DataTableModule,
        SharedModule,
        PaginatorModule,
        FormsModule,
        MessagesModule,
        ContextMenuModule,
        ToolbarModule,
        SplitButtonModule,
        LapDataGridModule,
        LapFormModule,
        GrowlModule,
        ButtonModule,
        AlertModule.forRoot()
    ],
    exports: [LapCrudComponent],
    entryComponents: [
        LapFormComponent,
        ConfirmDelete
    ],
    providers: [CrudService, PopupService, DataProvider, CheckFormEvent, ConfirmDeleteEvent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})


export class LapCrudModule {
}
