export class crudOptions {
    allowCreate?: boolean = false;
    allowDelete?: boolean = false;
    confirmDelete?: boolean = true;
    allowEdit?: boolean = false;
    multiSelect?: boolean = true;
    title?: string = '';
    source: any;
    fieldInMsg?: string = 'id';
    libelPlurielInMsg?: string = 'items';
    libelInMsg?: string = 'item';

    deleteConstraints?: Constraint[] = [];
    editConstraints?: Constraint[] = [];

    shield?: any;

    constructor(obj ?: any) {
        if(obj) {
            Object.keys(obj).forEach(key => {
                if (obj[key] && obj[key] != "''") {
                    this[key] = obj[key];
                }
            });
        }
    }
}

export interface Constraint {
    field: string;
    condition: any;
    message: string;
}
