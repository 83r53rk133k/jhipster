import { PhpInterfaceDbquery, PhpInterfaceRequeteur } from './lap-services/php.interface';

const BACK_JAVA = '';
const JAVA_ROOT = 'api/';

const ROUTE = [
    {env: 'shield', name: '', address: 'http://lce.local/'},
    {env: 'java', name: 'author', address: 'authors'},
    {env: 'java', name: 'book', address: 'books'},
    {env: 'java', name : 'user', address:'users'},
];

export class RouteController {
    getRoute(env: 'java' | 'shield', params?: string|PhpInterfaceRequeteur|PhpInterfaceDbquery) {
        if (env === 'shield') {
            let route = ROUTE.filter(elem => elem.env === env)[0];
            route['params'] = params;
            return route;
        } else {
            return {env: env, address: BACK_JAVA + JAVA_ROOT + ROUTE.filter(elem => elem.name === params)[0].address};
        }
    }
}
