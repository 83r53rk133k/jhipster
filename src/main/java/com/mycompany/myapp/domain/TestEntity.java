package com.mycompany.myapp.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TestEntity.
 */
@Entity
@Table(name = "test_entity")
public class TestEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "t")
    private String t;

    @ManyToOne(optional = false)
    @NotNull
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getT() {
        return t;
    }

    public TestEntity t(String t) {
        this.t = t;
        return this;
    }

    public void setT(String t) {
        this.t = t;
    }

    public User getUser() {
        return user;
    }

    public TestEntity user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestEntity testEntity = (TestEntity) o;
        if (testEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), testEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TestEntity{" +
            "id=" + getId() +
            ", t='" + getT() + "'" +
            "}";
    }
}
