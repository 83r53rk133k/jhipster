package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.TestEntity;
import com.mycompany.myapp.repository.TestEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing TestEntity.
 */
@Service
@Transactional
public class TestEntityService {

    private final Logger log = LoggerFactory.getLogger(TestEntityService.class);

    private final TestEntityRepository testEntityRepository;

    public TestEntityService(TestEntityRepository testEntityRepository) {
        this.testEntityRepository = testEntityRepository;
    }

    /**
     * Save a testEntity.
     *
     * @param testEntity the entity to save
     * @return the persisted entity
     */
    public TestEntity save(TestEntity testEntity) {
        log.debug("Request to save TestEntity : {}", testEntity);
        return testEntityRepository.save(testEntity);
    }

    /**
     *  Get all the testEntities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TestEntity> findAll(Pageable pageable) {
        log.debug("Request to get all TestEntities");
        return testEntityRepository.findAll(pageable);
    }

    /**
     *  Get one testEntity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TestEntity findOne(Long id) {
        log.debug("Request to get TestEntity : {}", id);
        return testEntityRepository.findOne(id);
    }

    /**
     *  Delete the  testEntity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TestEntity : {}", id);
        testEntityRepository.delete(id);
    }
}
