package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TestEntity;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the TestEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TestEntityRepository extends JpaRepository<TestEntity,Long> {

    @Query("select test_entity from TestEntity test_entity where test_entity.user.login = ?#{principal.username}")
    List<TestEntity> findByUserIsCurrentUser();

}
